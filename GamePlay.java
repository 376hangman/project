// This is the GamePlay class.
public class GamePlay {
	public GameLevel level;
	public Integer numGuess;
	public String word;

	public GamePlay(String level, String word) {
		System.out.println("Level is: " + level);
		switch(level)
		{
			case "Easy":
				this.level = GameLevel.EASY;
				this.numGuess = 25;
				break;
				
			case "Medium":
				this.level = GameLevel.MEDIUM;
				this.numGuess = 20;
				break;
				
			case "Hard":
				this.level = GameLevel.HARD;
				this.numGuess = 15;
				break;
			
			default:
				this.level = GameLevel.MEDIUM;
				this.numGuess = 20;
				break;
		}
		this.word = word;
	}
}
