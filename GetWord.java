import java.util.ArrayList;
import java.util.Random;


public class GetWord {

	 private static String [] words =   //choose secret word from these
		    {"geography", "cat", "yesterday", "java", "truck", "opportunity",
		        "fish", "token", "transportation", "bottom", "apple", "cake",
		        "remote", "pocket", "terminology", "arm", "cranberry", "tool",
		        "caterpillar", "spoon", "watermelon", "laptop", "toe", "toad",
		        "fundamental", "capitol", "garbage", "anticipate", "apple"};
	
	 
	 	private String secretWord;       // the chosen secret word
	    private ArrayList<Character> correctLetters;   // correct guesses
	    private ArrayList<Character> incorrectLetters; // incorrect guesses

	    int index = new Random().nextInt(words.length);
	    String randomWord = words[index];
	    char letter;
	    
	    public String getString(){
	    	return randomWord;
	    }
	    
	    
	    public static void matchLetter(String secret, StringBuffer dashes, char letter)
	      {
	         for (int index = 0; index < secret.length(); index++)
	            if (secret.charAt(index) == letter){
	               dashes.setCharAt(index, letter);
	            }
	         System.out.print("good guess - ");
	      }
	  
	    
	    public static StringBuffer makeDashes(String s)
	      {
	         StringBuffer dashes = new StringBuffer(s.length());
	         for (int count=0; count < s.length(); count++)
	            dashes.append('-');
	         return dashes;
	      }
}
