// This is the Hangman Server class.

// The following methods are used in this class: listen(), sendMsg(), receiveMsg() and close().

import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class HangmanServerCommunicator {
	int port_number;
	ServerSocket server_socket;
	Socket client_socket;

	public HangmanServerCommunicator( int port ) throws IOException {
		port_number= port;
		server_socket= new ServerSocket( port_number );
		
	}
	
	public void listen() throws IOException
	{
        System.out.println( "Listening on port " + Integer.toString( port_number ) );
		// listen for a connection
		client_socket= server_socket.accept();
        
        HangmanGame game = new HangmanGame(client_socket);
        game.start();
        
    }
    
    public void sendMsg(String msg)
    {
        try
        {
            DataOutputStream output = new DataOutputStream(client_socket.getOutputStream());
            output.writeBytes(msg);
            System.out.println(msg);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void receiveMsg()
    {
        try
        {
            // grab the input from server
		    BufferedReader input = new BufferedReader( 
				 new InputStreamReader(client_socket.getInputStream()) );
		
            // read the input
            String input_line= reader.readLine();
            System.out.println( "Received from client: " );
            System.out.println( input_line );
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
	}
	
	void close()
	{
		try
		{
			client_socket.close();
			System.out.println( "Listening concluded; shutting down..." );
			server_socket.close();
		}
		catch( Exception e )
		{
			// ignore
		}
	}
}
