// This WordHandler class chooses a random word from the file and has other methods that create dashes and check for letter matches.

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class WordHandler {
	String word;
	File file;
	String dashes;
	ArrayList<Character> correctLetters;
	
	public WordHandler() throws IOException {
		file = new File("words.txt");
		word = chooseWord();
	}
	

	public String chooseWord() throws IOException {
		ArrayList<String> words = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		String line;
		while ((line = reader.readLine()) != null) {
			words.add(line);
		}
		
		reader.close();
		
		Random rand = new Random();
		int randInd = rand.nextInt(words.size());
		
		String wordGuess = words.get(randInd);
		
		return wordGuess;
	}
    
    	    
	public static void matchLetter(String secret, StringBuffer dashes, char letter)
	      {
	         for (int index = 0; index < secret.length(); index++)
	            if (secret.charAt(index) == letter){
	               dashes.setCharAt(index, letter);
	            }
	         System.out.print("good guess - ");
	      }
	  
	    
	public static StringBuffer makeDashes(String s)
	      {
	         StringBuffer dashes = new StringBuffer(s.length());
	         for (int count=0; count < s.length(); count++)
	            dashes.append('-');
	         return dashes;
	      }
}
